
/*
	MANZANA v0.1
	---------------------------------------------
	Desenvolvido em CoffeeScript
 	por Fabiane Lima

	Licença: https://opensource.org/licenses/MIT
 */

(function() {
  var imgs, preload;

  imgs = ['assets/img/bg.png', 'assets/img/andando.gif', 'assets/img/down.svg', 'assets/img/intro.png', 'assets/img/parado.gif', 'assets/img/porta-abrindo.gif', 'assets/img/porta-fechando.gif', 'assets/img/porta.png', 'assets/img/up.svg', 'assets/img/audio.svg', 'assets/img/audio-off.svg'];

  preload = function(imgs) {
    var counter;
    counter = 0;
    $(imgs).each(function() {
      $('<img />').attr('src', this).appendTo('body').css({
        display: 'none'
      });
      return counter++;
    });
    if (counter === imgs.length) {
      $('main').css({
        opacity: '1'
      });
      return $('body').css({
        background: '#e7e7e7'
      });
    }
  };

  $(window).on('load', function() {
    return preload(imgs);
  });

  $(function() {
    var audio, clickarea, dragdrop, func, quiz, sets, slideshow, trueORfalse;
    sets = {
      audio: true,
      clickarea: false,
      quiz: true,
      trueORfalse: false,
      slideshow: false,
      dragdrop: false
    };
    audio = {
      trilha: new Audio('assets/audio/trilha.mp3'),
      clique: new Audio('assets/audio/clique.wav'),
      corrida: new Audio('assets/audio/run.mp3'),
      erro: new Audio('assets/audio/erro.mp3'),
      porta: new Audio('assets/audio/ring.mp3'),
      fechando: new Audio('assets/audio/close.mp3'),
      start: function() {
        if (sets.audio === true) {
          audio.trilha.volume = 0.35;
          audio.trilha.loop = true;
          audio.trilha.play();
          audio.clique.play();
          return $('.content').append('<button class="audio"><img src="assets/img/audio.svg"></button>');
        }
      },
      audio: function() {
        audio.clique.play();
        if (sets.audio === false) {
          sets.audio = true;
          audio.trilha.play();
          return $('.audio').html('<img src="assets/img/audio.svg">');
        } else if (sets.audio === true) {
          sets.audio = false;
          audio.trilha.pause();
          return $('.audio').html('<img src="assets/img/audio-off.svg">');
        }
      }
    };
    clickarea = {
      pro: void 0,
      count: 0,
      ctrl: [],
      resp: [],
      data: [
        {
          txt: '1. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
          pos: [50, 30]
        }, {
          txt: '2. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
          pos: [20, 20]
        }, {
          txt: '3. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
          pos: [10, 30]
        }, {
          txt: '4. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
          pos: [30, 70]
        }, {
          txt: '5. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
          pos: [50, 50]
        }
      ],
      start: function() {
        var i, k, len, ref, results;
        if (sets.clickarea === true) {
          $('.content').append('<div class="clickarea"></div>');
          ref = clickarea.data;
          results = [];
          for (k = 0, len = ref.length; k < len; k++) {
            i = ref[k];
            clickarea.resp.push(clickarea.data.indexOf(i));
            $('.clickarea').append('<div></div>');
            $('.clickarea div:nth-child(' + (clickarea.count + 1) + ')').css({
              top: clickarea.data[clickarea.count].pos[0] + '%',
              left: clickarea.data[clickarea.count].pos[1] + '%'
            });
            results.push(clickarea.count++);
          }
          return results;
        }
      },
      showC: function($el) {
        clickarea.ctrl.push($el.index());
        $el.css({
          pointerEvents: 'none',
          opacity: '0.6'
        });
        $('.dimmer').fadeIn();
        return $('.modal').html(clickarea.data[$el.index()].txt + '<button class="dismiss callend">Fechar</button>');
      },
      callEnd: function() {
        var i, j, k, len, ref, results;
        if (clickarea.ctrl.length !== clickarea.data.length) {
          return false;
        } else {
          ref = clickarea.ctrl;
          results = [];
          for (j = k = 0, len = ref.length; k < len; j = ++k) {
            i = ref[j];
            if (clickarea.ctrl.indexOf(i) === clickarea.resp[j]) {
              results.push(setTimeout(function() {
                return func.end();
              }, 1000));
            } else {
              results.push(void 0);
            }
          }
          return results;
        }
      }
    };
    quiz = {
      alt: void 0,
      pro: void 0,
      ctrl: [],
      count: 0,
      score: 0,
      error: 0,
      inOrder: 1,
      data: [
        {
          enun: 'Essa dimensão para evidenciar a necessidade de se delimitar períodos em que seja avaliado um desempenho. Isso não quer dizer que a avaliação de desempenho na empresa não deva ser permanente, pelo contrário, deve ser contínua e sistêmica em função dos objetivos a serem alcançados pela empresa.',
          alts: ['Tempo', 'Qualidade', 'Ocorrência', 'Amplitude', 'Natureza'],
          answ: 0
        }, {
          enun: 'Esta dimensão evidencia eficácia e eficiência, cujos conceitos devem ser bem claros em relação a uma atividade. A eficácia remete ao alcance de uma meta, de algo planejado e desejado. Já a eficiência remete à relação custo/benefício, recursos consumidos para atingir determinada meta planejada.',
          alts: ['Tempo', 'Qualidade', 'Ocorrência', 'Amplitude', 'Natureza'],
          answ: 1
        }, {
          enun: 'Essa dimensão tem como objetivo promover a eficácia e a eficiência, pois trata do momento que se realiza a avaliação de desempenho. Catelli (2010, p. 206) destaca que o desempenho planejado se refere a atividades a serem realizadas pela empresa e, conforme as definições de planejamento, comporta os estados futuros desejados para que o sistema atinja seus objetivos, ou seja, é aquele que deve orientar a realização do desempenho.',
          alts: ['Tempo', 'Qualidade', 'Ocorrência', 'Amplitude', 'Natureza'],
          answ: 2
        }, {
          enun: 'Nesta dimensão temos a abordagem funcional, que pode ser utilizada para o nível do colaborador ou de cargos: atividades desempenhadas pelos gerentes de negócio; desempenho funcional da área de produção. É o nível mais detalhado de desempenho e por isso muito utilizado para avaliação de pessoal.',
          alts: ['Tempo', 'Qualidade', 'Ocorrência', 'Amplitude', 'Natureza'],
          answ: 3
        }, {
          enun: 'Apesar de o foco da Controladoria ser o resultado econômico, não podemos nos esquecer de que o resultado econômico-financeiro decorre de uma boa execução operacional, assim, podemos dizer que os indicadores operacionais antecipam o que vai acontecer com os indicadores financeiros e econômicos. Qual é esta dimensão?',
          alts: ['Tempo', 'Qualidade', 'Ocorrência', 'Amplitude', 'Natureza'],
          answ: 4
        }
      ],
      rNumber: function() {
        quiz.randy = Math.floor(Math.random() * quiz.data.length);
        if (quiz.ctrl.length < quiz.data.length) {
          if (quiz.ctrl.indexOf(quiz.randy) === -1) {
            quiz.ctrl.push(quiz.randy);
            $('.quiz').append('<section id="' + quiz.randy + '" class="q"> <div class="andar">Térreo</div> <div class="feed"></div> <div class="enun"> <p>' + quiz.data[quiz.randy].enun + '</p> </div> <div class="alts"><ul></ul></div> </section>');
            quiz.pro = true;
          } else {
            quiz.pro = false;
          }
          quiz.putAlts(quiz.randy);
          return quiz.rNumber();
        }
      },
      start: function() {
        if (sets.quiz === true) {
          $('.content').append('<div class="quiz"></div>');
          return quiz.rNumber();
        }
      },
      selectAlt: function($el) {
        var answ, feedl, left;
        left = [28, 42, 55.5, 69, 82.7];
        feedl = [31.9, 45.7, 59.4, 73.1, 86.8];
        quiz.alt = $el.index();
        answ = void 0;
        $el.find('img').attr('src', 'assets/img/porta-abrindo.gif');
        $('.andar').fadeOut();
        $('.feed').css({
          left: feedl[quiz.alt] + '%'
        });
        if (quiz.alt === quiz.data[quiz.ctrl[quiz.count]].answ) {
          answ = true;
          $('.andar').html((quiz.ctrl.indexOf(quiz.alt) + 1) + '&ordm; andar');
          $('.feed').html('<img src="assets/img/up.svg">');
          $('.feed').fadeIn();
        } else {
          answ = false;
          $('.andar').html('Térreo');
          $('.feed').html('<img src="assets/img/down.svg">');
          $('.feed').fadeIn();
        }
        audio.corrida.play();
        audio.porta.play();
        $('.char').html('<img src="assets/img/andando.gif">');
        return $('.char').animate({
          left: left[quiz.alt] + '%'
        }, 1500, function() {
          $('.char').html('<img src="assets/img/parado.gif">');
          return $('.char').delay(500).fadeOut(function() {
            audio.fechando.play();
            $el.find('img').attr('src', 'assets/img/porta-fechando.gif');
            $('.dimmer').delay(1000).fadeIn(function() {
              return $('.andar').fadeIn();
            });
            $('.modal').html('<h1></h1><p>Clique no botão abaixo para continuar.</p><button class="nxt">Prosseguir</button>');
            if (answ === true) {
              quiz.score++;
              return $('.modal h1').html('Resposta certa!');
            } else {
              quiz.error++;
              audio.erro.play();
              return $('.modal h1').html('Resposta errada!');
            }
          });
        });
      },
      nxt: function() {
        quiz.count++;
        $('.feed').fadeOut();
        $('.char').css({
          display: 'block',
          left: '10%'
        });
        $('.char').html('<img src="assets/img/parado.gif">');
        if (quiz.count < quiz.data.length) {
          func.dismiss();
          quiz.putAlts();
          $('.quiz section:nth-child(' + quiz.count + ')').fadeOut();
          return $('.quiz section:nth-child(' + (quiz.count + 1) + ')').fadeIn();
        } else {
          return setTimeout(function() {
            return func.end();
          }, 400);
        }
      },
      putAlts: function(randy) {
        var testPromise;
        return testPromise = new Promise(function(resolve, reject) {
          if (quiz.pro === true) {
            return resolve();
          } else {
            return reject();
          }
        }).then(function(fromResolve) {
          var i, j, k, len, ref, results;
          ref = quiz.data[randy].alts;
          results = [];
          for (j = k = 0, len = ref.length; k < len; j = ++k) {
            i = ref[j];
            $('.quiz section:nth-child(' + quiz.inOrder + ') .alts ul').append('<li><p>' + i + '</p><img src="assets/img/porta.png"></li>');
            if (j === quiz.data[randy].alts.length - 1) {
              results.push(quiz.inOrder++);
            } else {
              results.push(void 0);
            }
          }
          return results;
        })["catch"](function(fromReject) {});
      }
    };
    trueORfalse = {
      count: 0,
      score: 0,
      alt: void 0,
      data: [
        {
          titl: 'Título da questão',
          text: '1. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
          answ: true,
          feed: 'Ut enim ad minim veniam, quis nostrud exercitation.'
        }, {
          titl: 'Título da questão',
          text: '2. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
          answ: true,
          feed: 'Ut enim ad minim veniam, quis nostrud exercitation.'
        }, {
          titl: 'Título da questão',
          text: '3. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
          answ: false,
          feed: 'Ut enim ad minim veniam, quis nostrud exercitation.'
        }, {
          titl: 'Título da questão',
          text: '4. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
          answ: false,
          feed: 'Ut enim ad minim veniam, quis nostrud exercitation.'
        }, {
          titl: 'Título da questão',
          text: '5. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
          answ: true,
          feed: 'Ut enim ad minim veniam, quis nostrud exercitation.'
        }
      ],
      start: function() {
        var i, j, k, len, ref, results;
        if (sets.trueORfalse === true) {
          $('.content').append('<div class="t-or-f"></div>');
          j = 0;
          func.dismiss();
          ref = trueORfalse.data;
          results = [];
          for (k = 0, len = ref.length; k < len; k++) {
            i = ref[k];
            $('.t-or-f').append('<section> <div class="txt"> <h1>' + trueORfalse.data[j].titl + '</h1> <p>' + trueORfalse.data[j].text + '</p> </div> <div class="ctrl"> <button class="true">verdadeiro</button> <button class="false">falso</button> </div> </section>');
            results.push(j++);
          }
          return results;
        }
      },
      verify: function($el) {
        $('.ctrl').css({
          pointerEvents: 'none'
        });
        if ($el.attr('class') === 'true') {
          trueORfalse.alt = true;
        } else if ($el.attr('class') === 'false') {
          trueORfalse.alt = false;
        }
        $('.dimmer').fadeIn();
        $('.modal').html('<h1></h1><p>' + trueORfalse.data[trueORfalse.count].feed + '</p><button class="nxt">Próxima</button>');
        if (trueORfalse.alt === trueORfalse.data[trueORfalse.count].answ) {
          trueORfalse.score++;
          return $('.modal h1').html('Resposta correta!');
        } else {
          return $('.modal h1').html('Resposta errada!');
        }
      },
      nxt: function() {
        trueORfalse.count++;
        if (trueORfalse.count < trueORfalse.data.length) {
          func.dismiss();
          $('.t-or-f .ctrl').css({
            pointerEvents: 'auto'
          });
          $('.t-or-f section:nth-child(' + trueORfalse.count + ')').fadeOut();
          return $('.t-or-f section:nth-child(' + (trueORfalse.count + 1) + ')').fadeIn();
        } else {
          return setTimeout(function() {
            return func.end();
          }, 500);
        }
      }
    };
    slideshow = {
      count: 0,
      data: [
        {
          img: 'assets/img/img1.png',
          txt: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.'
        }, {
          img: 'assets/img/img2.png',
          txt: 'Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
        }, {
          img: 'assets/img/img3.png',
          txt: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.'
        }, {
          img: 'assets/img/img4.png',
          txt: 'Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
        }, {
          img: 'assets/img/img5.png',
          txt: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.'
        }
      ],
      start: function() {
        var i, j, k, len, ref, results;
        if (sets.slideshow === true) {
          $('.content').append('<div class="slideshow"> <div class="slides"></div> <div class="ctrl"> <button class="next">></button> <button class="prev"><</button> </div> </div>');
          ref = slideshow.data;
          results = [];
          for (j = k = 0, len = ref.length; k < len; j = ++k) {
            i = ref[j];
            results.push($('.slides').append('<section> <div>' + slideshow.data[j].txt + '</div> <img src="' + slideshow.data[j].img + '"> </section>'));
          }
          return results;
        }
      },
      slide: function($el) {
        if ($el.attr('class') === 'next') {
          slideshow.count++;
          $('.prev').css({
            pointerEvents: 'auto',
            opacity: '1'
          });
          if (slideshow.count < slideshow.data.length) {
            $('.slides section').fadeOut();
            $('.slides section:nth-child(' + (slideshow.count + 1) + ')').fadeIn();
          } else {
            func.end();
          }
        }
        if ($el.attr('class') === 'prev') {
          slideshow.count--;
          $('.slides section').fadeOut();
          $('.slides section:nth-child(' + (slideshow.count + 1) + ')').fadeIn();
          if (slideshow.count === 0) {
            return $('.prev').css({
              pointerEvents: 'none',
              opacity: '0.6'
            });
          }
        }
      }
    };
    dragdrop = {
      count: 0,
      ctrl: [],
      endit: [],
      data: [['draggable 1', 'draggable 2', 'draggable 3', 'draggable 4', 'draggable 5', 'draggable 6'], ['draggable 1', 'draggable 2', 'draggable 3', 'draggable 4', 'draggable 5', 'draggable 6']],
      start: function() {
        var i, k, len, ref;
        if (sets.dragdrop === true) {
          $('.content').append('<div class="drag-drop"> <div class="draggie"></div> <div class="droppie"></div> </div>');
          func.dismiss();
          dragdrop.rNumber();
          ref = dragdrop.data[1];
          for (k = 0, len = ref.length; k < len; k++) {
            i = ref[k];
            $('.droppie').append('<div>' + i + '</div>');
          }
          dragdrop.draggie();
          return dragdrop.droppie();
        }
      },
      rNumber: function() {
        var randy;
        randy = Math.floor(Math.random() * dragdrop.data[0].length);
        if (dragdrop.ctrl.length < dragdrop.data[0].length) {
          if (dragdrop.ctrl.indexOf(randy) === -1) {
            dragdrop.ctrl.push(randy);
            $('.draggie').append('<div>' + dragdrop.data[0][randy] + '</div>');
          }
          return dragdrop.rNumber();
        }
      },
      draggie: function() {
        return $('.draggie').children().draggable({
          cursor: 'move',
          revert: function(event, ui) {
            this.data('uiDraggable').originalPosition = {
              top: 0,
              left: 0
            };
            return !event;
          }
        });
      },
      droppie: function() {
        return $('.droppie').children().droppable({
          tolerance: 'touch',
          accept: function(e) {
            if ($(this).html() === e.html()) {
              return true;
            }
          },
          drop: function(e, ui) {
            dragdrop.endit.push($(this).index());
            $('.ui-draggable-dragging').fadeOut();
            $(this).css({
              color: 'black',
              background: 'white',
              boxShadow: '0 0 0.5em rgba(0,0,0,0.6)'
            });
            if (dragdrop.endit.length === dragdrop.ctrl.length) {
              $('.draggie').fadeOut();
              return setTimeout(function() {
                return func.end();
              }, 800);
            }
          }
        });
      }
    };
    func = {
      help: function() {
        audio.clique.play();
        $('.dimmer').fadeIn();
        return $('.modal').html('<h1>Ajuda</h1><p>Clique na porta de elevador correta conforme o conceito apresentado. Mas tenha cuidado: se errar, volta ao início!</p><button class="dismiss">Fechar</button>');
      },
      info: function() {
        audio.clique.play();
        return $('.modal').html('<h1>Referência</h1><p>CATELLI, Armando (Coord.). <strong>Controladoria</strong>: uma abordagem da gestão econômica – Gecon. 2. ed. 8. reimpr. São Paulo: Atlas, 2010.</p><button class="end">Voltar</button>');
      },
      end: function() {
        audio.clique.play();
        $('.enun').fadeOut();
        $('.dimmer').delay(1000).fadeIn();
        $('.modal').html('<h1>Finalizando...</h1><p>A avaliação de desempenho é uma função essencial da Controladoria, e as dimensões proporcionam uma visão integral do desempenho empresarial.</p><button class="info">Referência</button>&nbsp;&nbsp;<button class="again">Ver novamente</button>');
        if (sets.quiz === true) {
          if (quiz.score > 1) {
            $('.modal h1').html('Você acertou ' + quiz.score + ' questões!');
          } else if (quiz.score < 1) {
            $('.modal h1').html('Você não acertou nenhuma questão!');
          } else {
            $('.modal h1').html('Você acertou uma questão!');
          }
        }
        if (sets.trueORfalse === true) {
          if (trueORfalse.score < 1) {
            return $('.modal h1').html('Você não acertou nenhuma questão!');
          } else if (trueORfalse.score > 1) {
            return $('.modal h1').html('Você acertou ' + trueORfalse.score + ' questões!');
          } else if (trueORfalse.score === 1) {
            return $('.modal h1').html('Você acertou uma questão!');
          }
        }
      },
      dismiss: function() {
        audio.clique.play();
        return $('.dimmer').fadeOut();
      },
      intro: function() {
        $('.intro').fadeOut();
        return $('.modal').delay(500).fadeIn();
      },
      start: function() {
        audio.start();
        clickarea.start();
        quiz.start();
        slideshow.start();
        trueORfalse.start();
        dragdrop.start();
        func.dismiss();
        return $('.content').fadeIn();
      }
    };
    $(document).on('click', '.audio', function() {
      return audio.audio();
    });
    $(document).on('click', '.clickarea *', function() {
      if (sets.clickarea === true) {
        return clickarea.showC($(this));
      }
    });
    $(document).on('click', '.dismiss', function() {
      if (sets.clickarea === true) {
        return clickarea.callEnd();
      }
    });
    $(document).on('click', '.alts li', function() {
      if (sets.quiz === true) {
        return quiz.selectAlt($(this));
      }
    });
    $(document).on('click', '.verify', function() {
      if (sets.quiz === true) {
        return quiz.verify();
      }
    });
    $(document).on('click', '.nxt', function() {
      if (sets.quiz === true) {
        return quiz.nxt();
      }
    });
    $(document).on('click', '.ctrl *', function() {
      if (sets.slideshow === true) {
        return slideshow.slide($(this));
      }
    });
    $(document).on('click', '.true, .false', function() {
      if (sets.trueORfalse === true) {
        return trueORfalse.verify($(this));
      }
    });
    $(document).on('click', '.nxt', function() {
      if (sets.trueORfalse === true) {
        return trueORfalse.nxt();
      }
    });
    $(document).on('click', '.int', function() {
      return func.intro();
    });
    $(document).on('click', '.start', function() {
      return func.start();
    });
    $(document).on('click', '.help', function() {
      return func.help();
    });
    $(document).on('click', '.info', function() {
      return func.info();
    });
    $(document).on('click', '.end', function() {
      return func.end();
    });
    $(document).on('click', '.dismiss', function() {
      return func.dismiss();
    });
    return $(document).on('click', '.again', function() {
      return location.reload();
    });
  });

}).call(this);
